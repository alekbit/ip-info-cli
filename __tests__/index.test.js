import getGeoInfoByIp from '../src';

it('works with async/await', async () => {
  const expected = 'Хабаровск, Хабаровский край, Россия, RU — 37.146.124.156, Asia/Vladivostok';

  const data = {
    as: 'AS3216 PJSC "Vimpelcom"',
    city: 'Хабаровск',
    country: 'Россия',
    countryCode: 'RU',
    isp: 'Corbina Khabarovsk Broadband',
    lat: 48.5027,
    lon: 135.066,
    org: '',
    query: '37.146.124.156',
    region: 'KHA',
    regionName: 'Хабаровский край',
    status: 'success',
    timezone: 'Asia/Vladivostok',
    zip: '',
  };

  const httpModuleMock = jest.fn().mockResolvedValue({ data });

  const actual = await getGeoInfoByIp('', httpModuleMock);

  expect(actual).toBe(expected);
});
