install:
	npm install

start:
	npx babel-node  ./src/bin/geo-bin.js 24.48.0.1

start_default:
	npx babel-node  ./src/bin/geo-bin.js


test:
	npm test

lint:
	npx eslint .

build:
	rm -rf dist
	npm run build

