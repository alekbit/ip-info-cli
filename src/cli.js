import program from 'commander';
import { description, version } from '../package.json';
import getGeoInfoByIp from '.';

program
  .description(description)
  .version(version)
  .arguments('[ip]')
  .action(async (ip = '') => {
    const result = await getGeoInfoByIp(ip);
    console.log();
    console.log(result);
    console.log();
  });

export default () => {
  program.parse(process.argv);
};
