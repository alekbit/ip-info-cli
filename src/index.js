import axios from 'axios';

export default async (ip, httpModule = axios) => {
  const { data } = await httpModule({
    method: 'get',
    url: `http://ip-api.com/json/${ip}`,
    params: {
      lang: 'ru',
    },
  });

  const {
    city, country, countryCode, query, regionName, timezone,
  } = data;

  return `${city}, ${regionName}, ${country}, ${countryCode} — ${query}, ${timezone}`;
};
